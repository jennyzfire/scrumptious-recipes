from django.db import models

class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=False)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name} Recipe by {self.author}"


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.name}"


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)


    def __str__(self):
        return f"{self.name}"


class Ingredient(models.Model):  # needs to be a combination of measures and fooditems
    food = models.ForeignKey(
        "FoodItem",
        related_name="FoodItems",
        on_delete=models.PROTECT
    )
    measure = models.ForeignKey(
        "Measure",
        related_name="Measures",
        on_delete=models.PROTECT
    )

    recipe = models.ForeignKey(
        "Recipe",
        related_name="Ingredients",
        on_delete=models.CASCADE
    )

    amount = models.FloatField(null=True)

    def __str__(self):
        amount = str(self.amount)
        measure = self.measure.name
        food = self.food.name
        return f"{amount} {measure} of {food}"


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe",
        related_name="Recipes",
        on_delete=models.CASCADE
    )
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField(
        "FoodItem",
        null=True,
        blank=True
    )

    def __str__(self):
        return f"Step {self.order}: for the {self.recipe} Recipe"
