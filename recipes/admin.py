from django.contrib import admin
from .models import Recipe, FoodItem, Measure, Ingredient, Step
# Register your models here.

admin.site.register(Recipe)
admin.site.register(FoodItem)
admin.site.register(Measure)
admin.site.register(Ingredient)
admin.site.register(Step)
